﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchDemo          // homme siit jätkame
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ma tahaks jagada");

            try
            {
                Console.Write("anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());

                Console.Write("anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());

                if (teine == 0) throw new Exception("nulliga meie jagada ei oska");
                int tulemus = yks / teine;
                

                Console.WriteLine($"tulemus {tulemus}");
            }
            catch (Exception e)
            {
                Console.WriteLine("midagi juhtus täna ei jagu");
                Console.WriteLine(e.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("tänaseks lõpetame jagamise");
            }

        }
    }
}
