﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom loom1 = new Loom();
            //Console.WriteLine(loom1);
            Loom loom2 = new Loom("krokodill");
            //Console.WriteLine(loom2);

            Koduloom kl = new Koduloom("prussakas") { Nimi = "Albert" };
            //Console.WriteLine(kl);
            //loom1.TeeHäält();
            //kl.TeeHäält();

            List<Loom> loomaaed = new List<Loom>
            {
                loom1,
                new Kass {Nimi = "Miisu"},
                loom2,
                kl,
                //new Koduloom("Kass") {Nimi = "kräänu"}, // ära nii tee
                new Kass {Nimi = "Garfield"},
            };

            //Kass k = (Kass)loomaaed[3];
            //k.Silita();

            //((Kass)loomaaed[2]).Silita();
            //var xxx = loomaaed[0];
            // if (typeof(Kass) == xxx.GetType());   //loomaaed[0] is Kass

            // enne silitamist kontrolli, kas on tegu Kassiga
            //if (loomaaed[2] is Kass) ((Kass)loomaaed[2]).Silita();
            //if (loomaaed[2] is Kass k) k.Silita();
            //(loomaaed[2] as Kass)?.Silita();
            
            // (loomaaed[2] is Kass ? (Kass)loomaaed[2] : null)
            // (loomaaed[2] as Kass)

            foreach (var x in loomaaed)
            {
                //if (x is Kass xk) xk.Silita();
                (x as Kass)?.SikutaSabast();
                x.TeeHäält();
            }

            Sepik sepik = new Sepik();
            Koer pauka = new Koer { Nimi = "Pauka" };

            Lõuna(sepik);
            Lõuna(pauka);

            Koer[] koerad =
            {
                pauka,
                new Koer {Nimi = "Polla"},
                new Koer {Nimi = "Muki"},
                new Koer {Nimi = "Adalbert"},
                new Koer {Nimi = "Pontu"},

            };
            Array.Sort(koerad);
            foreach (var x in koerad)
            {
                Console.WriteLine(x);
            }





        }

        static void Lõuna(ISöödav x)
        {
            x.Süüakse();
        }
    }
    // ma lihtsuse mõttes panen kõik klassid "ühte patta" - võiks panna eraldi failidena
    // ma lihtsuse mõttes kasutan HÄSTI lihtsaid klasse - praktilises elus on keerukamad

    abstract class Elajas
    {
        public abstract void Söömine(Elajas e);
    }

    class Loom: Elajas
    {
        public string Liik { get; private set; }
        public Loom(string liik)
        {
            Liik = liik;
        }
        public Loom() : this("tundmatu")  // sidumine C#-s
        {
            // this("tundmatu");  // konstruktorite sidumine Javas 
        }

        public virtual void TeeHäält()
        {
            Console.WriteLine($"{Liik} teeb koledat häält");
        }

        public override string ToString() => $"loom {Liik} liigist";

        public override void Söömine(Elajas e)
        {
            Console.WriteLine($"{this} sööb {e}");
        }
    }

    class Koduloom : Loom
    {
        public string Nimi { get; set; }

        public Koduloom(string liik) : base(liik)  // sidumine baasklassi konstruktoriga
        {
            // super(liik); // sidumine baasklassi (e superklassiga) Javas
            // konstruktor peab igal oma olema
        }

        public override void TeeHäält()
        {
            //base.TeeHäält();
            Console.WriteLine($"{Liik} {Nimi} häälitseb mõnusalt");
        }

        public override string ToString() => $"{Liik} {Nimi}";
    }

    class Kass : Koduloom
    {
        private bool tuju = false;
        public string Tõug { get; set; }

        public Kass() : base("kass") { }

        public void Silita() => tuju = true;
        public void SikutaSabast() => tuju = false;

        public override void TeeHäält()
        {
            Console.WriteLine(tuju ? $"{Nimi} lööb nurrru" : $"{Nimi} kräunub" );
        }
    }

    class Koer : Koduloom, ISöödav, IComparable   
    {
        public string Tõug { get; set; }
        public Koer() : base("koer") { }

        public void Süüakse()
        {
            Console.WriteLine($"koer {Nimi} pannakse nahka"); 
        }

        public void JäetakseSöömata()
        {
            throw new NotImplementedException();
        }

        public bool KasMaitses()
        {
            throw new NotImplementedException();
        }

        public int CompareTo(object obj)
        {
            return obj is Koer k ? Nimi.CompareTo(k.Nimi) : 1;
        }
    }

    interface ISöödav
    {
        void Süüakse();
        void JäetakseSöömata();
        bool KasMaitses();
    }

    class Sepik : ISöödav
    {
        public void JäetakseSöömata()
        {
            throw new NotImplementedException();
        }

        public bool KasMaitses()
        {
            throw new NotImplementedException();
        }

        public void Süüakse()
        {
            Console.WriteLine("Keegi nosib sepikut"); 
        }
    }

}
