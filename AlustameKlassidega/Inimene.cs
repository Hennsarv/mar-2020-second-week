﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    class Inimene
    {
        // andmed, baidid, bitid, nende tähendus
        public string Nimi; // vaikimisi tühi string
        public int Vanus = 18; // vaikimisi 0
        public Inimene Kaasa; // vaikimisi null


        // funktsionaalsus - mis tehted selles klassis on
        // vaatame hiljem seda poolt täpsemalt
        public override string ToString() => $"Inimene {Nimi} vanusega {Vanus}";


    }

   

}
