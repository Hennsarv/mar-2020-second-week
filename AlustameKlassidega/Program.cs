﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{

    
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene();
            henn.Nimi = "Henn Sarv";
            henn.Vanus = 64;

            Inimene ants = new Inimene() { Nimi = "Ants Saunamees", Vanus = 80 };

            List<Inimene> rahvas = new List<Inimene>
            {
                new Inimene{Nimi = "Joosep", Vanus = 118},
                new Inimene{Nimi = "Teele"},
                new Inimene{Nimi = "Arno"},
                henn,
                ants,
                new Inimene{Nimi = "Maris", Kaasa = henn},
            };



            Console.WriteLine(henn);

            MinuLoomad.Koer koer = new MinuLoomad.Koer();

            Inimene teine = henn; // omistamine muutuja ja avaldis sama tüüpi
            teine.Nimi = "Sarvik";

            Console.WriteLine(henn);

            Inimene[] inimesed = new Inimene[10]; // kümme inimest massiivis
            List<Inimene> teised = new List<Inimene>();

            teised.Add(new Inimene { Nimi = "Ants", Vanus = 28 });
            teised.Add(new Inimene { Nimi = "Peeter" });
            teised.Add(henn);
            henn.Kaasa = new Inimene { Nimi = "Maris", Kaasa = henn };

            // ?-tehteid vaatame kunagi hiljem - ära sellele hetkel keskendu
            foreach (var x in teised) 
                Console.WriteLine($"{x} ja tema kaasa on {x.Kaasa?.Nimi??"kaasa puudub"}");



        }
    }

    

}
