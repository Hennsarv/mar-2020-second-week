﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpilasteNimekiri
{
    

    class Program
    {
        static void Main(string[] args)
        {
            
            List<Inimene> õpilased = new List<Inimene>();
            try
            {

                foreach (var rida in System.IO.File.ReadAllLines("..\\..\\Õpilased.txt"))
                {
                    var osad = rida.Replace(", ", ",").Split(',');
                    if (osad.Length > 1) õpilased.Add(new Inimene { Nimi = osad[1], Isikukood = osad[0] });
                }

                foreach (var i in õpilased) Console.WriteLine($"{i} vanus on {i.Vanus()}");
            }
            catch (Exception)
            {

                Console.WriteLine("me vist ei leidnud faili üles");
            }

        }
    }


}
