﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpilasteNimekiri
{
    class Inimene
    {
        public string Nimi;
        public string Isikukood;

        // ta tahtis sünniaega ja vanust
        // vanust juba oskame, aga sünniajaga on keerukas

        public DateTime Sünniaeg()
        {
            int sajand = 1800;
            switch (Isikukood.Substring(0, 1))
            {
                case "3": case "4": sajand = 1900; break;
                case "5": case "6": sajand = 2000; break;
            }

            return new DateTime(
                // seda rida kasutasin alguses "jõuga"
                //2000 + // 2000 ei ole õige, kui on 3-4 siis 1900 ja kui 1-2 siis 1800
                
                // Henn tohib nii
                //(Isikukood[0]-'1') / 2 * 100 + 1800 + // ära jumala pärast nii tee
                
                // kui enne switchiga välja arvutada
                sajand +

                // variant Elvistega
                //(Isikukood[0] == '1' || Isikukood[0] == '2' ? 1800 :
                // Isikukood[0] == '3' || Isikukood[0] == '4' ? 1900 : 2000) + 

                int.Parse(Isikukood.Substring(1,2)),     // siia tuleks panna aasta
                int.Parse(Isikukood.Substring(3,2)),     // siia tuleks panna kuu
                int.Parse(Isikukood.Substring(5,2))      // sii atuleks panna päev
                );
        }

        public int Vanus() => ( DateTime.Today - Sünniaeg()).Days * 4 / 1461;


        public override string ToString()
        => $"{Isikukood} - {Nimi}";
    }
}
