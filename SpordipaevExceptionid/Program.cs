﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpordipaevExceptionid
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "spordipäeva protokoll.txt";
            string folder = ".";
            string[] failisisu;
            while (true)
            {
                try
                {
                    failisisu = System.IO.File.ReadAllLines($"{folder}\\{filename}");
                    break;
                }
                catch (Exception)
                {
                    Console.Write("kus su fail on: ");
                    folder = Console.ReadLine();
                }
            }
            // Console.WriteLine(string.Join("\n", failisisu));  // testimiseks, et kas sai fail loetud
            List<string> vigasedRead = new List<string>();
            List<Tulemus> tulemused = new List<Tulemus>();
            for (int i = 1; i < failisisu.Length; i++)
            {
                try
                {
                    var osad = failisisu[i].Split(',');
                    tulemused.Add(new Tulemus { Nimi = osad[0], Aeg = double.Parse(osad[2]), Distants = double.Parse(osad[1]) });
                }
                catch (Exception e)
                {
                    vigasedRead.Add($"{i + 1}. {failisisu[i]} # {e.Message}");
                }
            }

            vigasedRead.ForEach(x => Console.WriteLine(x));
        }
    }

    class Tulemus
    {
        public string Nimi { get; set; }
        public double Distants { get; set; }
        public double Aeg { get; set; }

        public double Kiirus => Distants / Aeg;
    }
}
