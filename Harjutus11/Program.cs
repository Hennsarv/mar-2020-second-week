﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Harjutus11
{
    class Program
    {
        static void Main(string[] args)
        {

            //Console.WriteLine(new Inimene{ Nimi = "Henn Sarv", IK = "35503070211"});
            //Console.WriteLine(new Õpilane { Nimi = "Toomas Linnupoeg", IK = "35505050000", Klass = "7C" });
            //Console.WriteLine(new Õpetaja { Nimi = "Malle Maasikas", IK = "44404040000", Aine = "matemaatika" });
            //Console.WriteLine(new Õpetaja { Nimi = "Tiit Tuvi", IK = "34404040000", Aine = "füüsika", Klass = "7C" });

            List<Inimene> koolipere = new List<Inimene>
            {
                new Inimene { Nimi = "Henn Sarv", IK = "35503070211" },
                new Õpilane { Nimi = "Toomas Linnupoeg", IK = "35505050000", Klass = "7C" },
                new Õpetaja { Nimi = "Malle Maasikas", IK = "44404040000", Aine = "matemaatika" },
                new Õpetaja { Nimi = "Tiit Tuvi", IK = "34404040000", Aine = "füüsika", Klass = "7C" }
            };
            var õpsid = System.IO.File.ReadAllLines("..\\..\\Õpetajad.txt");
            foreach(var õ in õpsid)
            {
                string[] osad = (õ + ",,").Split(',');
                koolipere.Add(new Õpetaja { IK = osad[0], Nimi = osad[1].Trim(), Aine = osad[2].Trim(), Klass = osad[3].Trim() });
            }

            õpsid = System.IO.File.ReadAllLines("..\\..\\Õpilased.txt");
            foreach (var õ in õpsid)
            {
                string[] osad = (õ + ",,").Split(',');
                koolipere.Add(new Õpilane { IK = osad[0], Nimi = osad[1].Trim(), Klass = osad[2].Trim() });
            }
            foreach (var x in koolipere) Console.WriteLine(x);

            var pere = JsonConvert.SerializeObject(koolipere);
            System.IO.File.WriteAllText("..\\..\\koolipere.json", pere);


            string vahva = new string('-', 20);
            Console.WriteLine(vahva);

            //for (int i = 0; i < 10000; i++)
            //{
            //    System.Threading.Thread.Sleep(10);
            //    Console.Write("\r[" + new string('.', i / 100) + new string(' ', 100-i/100) + "]");
            //}

            while (true) Console.Write($"\r{DateTime.Now:ss.fffff}");

        }
    }

    enum Sugu { Naine, Mees }

    class Inimene
    {
        public string IK { get; set; }
        public string Nimi { get; set; }

        public Sugu Sugu => (Sugu)(IK[0] % 2);

        public override string ToString() => $"{Sugu} {Nimi} ({IK})";

    }

    class Õpilane : Inimene
    {
        public string Klass { get; set; } // klass, kus ta õpib
        public override string ToString() => $"{Klass} klassi õpilane {Nimi} ({IK})";

    }

    class Õpetaja : Inimene
    {
        public string Aine { get; set; } // aine mida ta õpetab
        public string Klass { get; set; } = ""; // klass, mida juhatab

        public override string ToString() => $"{Aine} õpetaja {Nimi} ({IK})" +
            (Klass == "" ? "" : $" / {Klass} klassi juhataja");

    }
}
