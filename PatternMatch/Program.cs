﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PatternMatch
{

    // seda ära enne esmaspäeva vaata
    // ma pisut progen ise ka natuke
    static class E
    {
        public static double Pindala(this Kujund k)
        {
            switch (k)
            {
                case Ruut ru when (ru.Külg > 0):
                    return ru.Külg * ru.Külg;
                case Ring ri when (ri.Raadius > 0):
                    return ri.Raadius * ri.Raadius * Math.PI;
                case Ristkülik rk when (rk.Kõrgus > 0 && rk.Laius > 0):
                    return rk.Kõrgus * rk.Laius;
                case Kolmnurk ko when (ko.Alus > 0 && ko.Kõrgus > 0):
                    return ko.Kõrgus * ko.Alus / 2;
                default:
                    return 0;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Kujund> kujundid = new List<Kujund>
            {
                new Ruut { Külg = 10},
                new Ristkülik { Kõrgus = 3, Laius = 5},
                new Kolmnurk {Alus = 6, Kõrgus = 5},
                new Ring {Raadius = 10}
            };

            Console.WriteLine($"Kogupindala on {kujundid.Sum(x => x.Pindala())}");
        }
    }

    class Kujund
    {
        string värv { get; set; }
    }

    class Ruut : Kujund
    {
        public double Külg { get; set; }
    }

    class Ristkülik : Kujund
    {
        public double Laius { get; set; }
        public double Kõrgus { get; set; }
    }

    class Kolmnurk : Kujund
    {
        public double Alus { get; set; }
        public double Kõrgus { get; set; }
    }

    class Ring : Kujund
    {
        public double Raadius { get; set; }

    }



}



  
    

