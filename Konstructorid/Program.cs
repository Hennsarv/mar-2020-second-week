﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstructorid
{
    class Program
    {
        static void Main(string[] args)
        {
            //new Inimene("35503070211") { Nimi = "Henn" };
            //new Inimene("35503070211") { Nimi = "Sarvik" }; // teisel korral sama ID-d nimestikku ei panda
            //new Inimene("50312311000", "Ants");

            Inimene.Create("35503070211").Nimi = "Henn";

            Console.WriteLine(Inimene.Leia("35503070211") ?. Nimi.ToUpper() ?? "sihukest pole"); 
            Console.WriteLine(Inimene.Leia("35503070212") ?. Nimi.ToUpper() ?? "sihukest pole");

            // ühesilmaga elvis
            Console.WriteLine(
            Inimene.Leia("35503070211") == null ? "siukest põle" : Inimene.Leia("35503070211").Nimi.ToUpper()
            );

            // avaldis ?? "asendus"      // colaesce tehe
            // avaldis == null ? "asendus" : avaldis

            string test = "*";
            while (test != "")
            try
            {
                Console.Write("anna oma isikukood: ");
                if (Inimene.TryCreate2(test = Console.ReadLine(), out Inimene i))
                {
                    Console.Write("ja sinu nimi: ");
                    i.Nimi = Console.ReadLine();
                }
                else Console.WriteLine("vigane või korduv isikukood");
            }
            catch (Exception e)
            {
                
                Console.WriteLine($"jätame selle lisamata, kuna {e.Message}");
                    
            }


            foreach (var x in Inimene.Inimesed) Console.WriteLine(x.Nimi + ":" + x.IK + ":" + x.Sünniaeg.ToShortDateString());
        }
    }

    class Inimene
    {
        static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;
        static int loendur;

        public int Nr {get;} = ++loendur;
        public string Nimi { get; set; }
        public readonly string IK;
        public DateTime Sünniaeg { get; private set; }

        //Inimene(string ik)
        //{ 
        //    IK = ik;
        //    
        //}

        Inimene(string ik, string nimi = "nime veel pole", DateTime datetime = new DateTime())  // overloaded construktor - kaks sama nimega asja
            //: this(ik)  // konstruktorite sidumine C#
        {
            // this(ik); // konstruktorite sidumine Javas
            IK = ik;
            Nimi = nimi;
            _Inimesed.Add(ik, this);
            Sünniaeg = datetime;
        }

        public static Inimene Create(string ik, string nimi = "")
        => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] :  new Inimene(ik, nimi);
        
        public static Inimene Leia(string ik) => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : null;

        public static bool TryCreate(string ik, out Inimene inimene)
        
            => (inimene = (ik.Length == 11 && !_Inimesed.ContainsKey(ik) && DateTime.TryParse(
                (ik[0] == '1' || ik[0] == '2' ? "18" :
                ik[0] == '3' || ik[0] == '4' ? "19" :
                ik[0] == '5' || ik[0] == '6' ? "20" : "xx") +
                ik.Substring(1, 2) + "/" +
                ik.Substring(3, 2) + "/" +
                ik.Substring(5, 2)
                , out DateTime d)) ? new Inimene(ik, "", d) : null) != null; 

        public static bool TryCreate2(string ik, out Inimene inimene)
        {
            inimene = null;
            if (ik == "") return false;
            if (ik.Length != 11) throw new Exception("isikukood vale pikkusega");
            if (_Inimesed.ContainsKey(ik)) throw new Exception($"Selline isikukood {ik} juba on");
            
                string kp = "xx";
                switch(ik[0])
                {
                    case '1': case '2':
                        kp = "18";
                        break;
                    case '3': case '4':
                        kp = "19";
                        break;
                    case '5': case '6':
                        kp = "20";
                        break;
                }
                kp += ik.Substring(1, 2) + "/" + ik.Substring(3, 2) + "/" + ik.Substring(5, 2);
            if (DateTime.TryParse(kp, out DateTime d))
            {
                inimene = new Inimene(ik, "", d);
                return true;
            }
            else throw new Exception($"isikukood {ik} vigane");   
            
            

        }
    }
}
