﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetodidJaFunktsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            #region esimene näiteplokk
            //DateTime sünnipäev = new DateTime(1955, 3, 7);

            //Trüki(sünnipäev);  // meetodi väljakutse

            //Console.WriteLine(Inimene.Vanus(sünnipäev));  // funktsiooni kasutamine (avaldises)

            //if (Inimene.Vanus(sünnipäev) > 18) Console.WriteLine("talle võib viina müüa"); 
            #endregion

            Inimene henn = new Inimene { Nimi = "Henn", Sünniaeg = new DateTime(1955, 3, 7) };
            Inimene ants = new Inimene { Nimi = "Ants", Sünniaeg = DateTime.Parse("17. aprill 2000") };

            Console.WriteLine(henn);
            Console.WriteLine(ants);

            //Console.WriteLine(henn.AgeInstants());

            //Console.WriteLine(Inimene.AgeStatic(ants));

            List<Inimene> kahekesi = new List<Inimene>
            {
                henn, ants,
            };

            foreach (var x in kahekesi) x.AgeInstants();

            for (int i = 0; i < kahekesi.Count; i++)
                Console.WriteLine(kahekesi[i].AgeInstants());
            
        
        }

        

        static void Trüki(DateTime päev) // meetod
        {
            Console.WriteLine(päev.ToString("(ddd) dd.MMMM.yyyy"));
      
        }

    }

    class Inimene
    {
        static int inimesteArv = 0;
        static int adult = 18;

        public int Number = ++inimesteArv;
        public string Nimi;
        public DateTime Sünniaeg;

        public bool KasTäiSealine() { return AgeInstants() > adult; }

        public override string ToString()
        => $"{Number}. {Nimi} kes on sündinud {Sünniaeg:dd.MMMM.yyyy}";


        // henn.AgeInstants()
        public int AgeInstants() => Vanus(this.Sünniaeg);

        // Inimene.AgeStatic(ants)
        public static int AgeStatic(Inimene kes) => Vanus(kes.Sünniaeg);



        internal static int Vanus(DateTime päev)  // funktsioon
        {
            return (DateTime.Today - päev).Days * 4 / 1461;
        }

    }

    

}
