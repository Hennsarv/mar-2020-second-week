﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjutus9
{

    enum Gender { Naine, Mees }
    enum ChildGender { Tüdruk, Poiss }

    class Person
    {
        public string IK { get; set; } = "";
        public string Nimi { get; set; }

        public Gender Gender => (Gender)(IK[0] % 2); 
        public ChildGender ChildGender => (ChildGender)(IK[0] % 2);

        public string GenderToString => Age >= 20 ? Gender.ToString() : ChildGender.ToString();

        public DateTime BirthDate =>
            IK == "" ? new DateTime() :
            new DateTime(
                (IK[0] == '5' || IK[0] == '6' ? 2000 :    // sajand +
                 IK[0] == '3' || IK[0] == '4' ? 1900 :    // sajand +
                 1800) +
                int.Parse(IK.Substring(1, 2)),    // aasta
                int.Parse(IK.Substring(3, 2)),    // kuu
                int.Parse(IK.Substring(5, 2))    // päev
                );

        public int Age => (DateTime.Today - BirthDate).Days * 4 / (365 * 4 + 1);
        //public int VanusO() => (DateTime.Today - this.BirthDate).Days * 4 / (365 * 4 + 1);
        //public static int VanusC(Person p) => (DateTime.Today - p.BirthDate).Days * 4 / (365 * 4 + 1);

        public override string ToString()
        => $"{GenderToString} {Nimi} vanusega {Age} a";
        

    }
}
