﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Harjutus9
{
    static class MyFunctions
    {
        public static int Vanus(this Person p) => (DateTime.Today - p.BirthDate).Days * 4 / (365 * 4 + 1);

        public static string ToProper(this string s)
            => s == "" ? "" :
                s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();

        public static string[] ReadAllLines(this string filename)
            => System.IO.File.ReadAllLines(filename);

        public static int Liida(int a, int b)
        {
            return a + b;
        }

        public static int Ruut(this int x) => x * x;

        public static int Tryki(this int a, string nimi)
        {
            Console.WriteLine($"{nimi}: {a}");
            return a;
        }

        public static void Arvuta(int x, int y, out int summa, out int korrutis)
        {
            summa = x + y;
            korrutis = x * y;
        }

        public static (int, int) Arvuta(int x, int y)
        {
            return (x + y, x * y);
        }

        public static void Vaheta(ref int x, ref int y)
        {
            //int ajutine = x;
            //x = y;
            //y = ajutine;
            (x, y) = (y, x);
        }

        //public static void Vaheta(ref string x, ref string y) => (x, y) = (y, x);

        public static void Swap<T>(ref T x, ref T y) => (x, y) = (y, x);

        public static int Factorial(int x)
            => x < 2 ? 1 : x * Factorial(x - 1);

        public static int Fact(int x)
        {
            int vastus = 1;
            while (x > 1) vastus *= x--;
            return vastus;
        }

    }


    class Program
    {
        static void Main(string[] args)
        {

           
            DateTime minusünnipäev = new DateTime(1955, 3, 7);
            DateTime täna = DateTime.Today;

            MyFunctions.Swap(ref minusünnipäev, ref täna);


            int arv = 77;
            arv.Tryki("arv").Ruut().Tryki("ruut");

            (77.Tryki("sietse").Ruut() + 100.Tryki("lisasin 100")).Tryki("tulemus");

            MyFunctions.Tryki(arv, "arv");
            MyFunctions.Tryki(MyFunctions.Liida(7, 4), "tulemus");

            int s, k;
            MyFunctions.Arvuta(7, 5, out s, out k);
            MyFunctions.Swap(ref s, ref k);


            Console.WriteLine("keerulinesõna".ToProper());

            MyFunctions.Vaheta(ref k, ref s);

            Person henn = new Person { IK = "35503070211", Nimi = "Henn" };
            //Console.WriteLine(henn);

            //Console.WriteLine(henn.VanusO());
            Console.WriteLine(MyFunctions.Vanus(henn));
            Console.WriteLine(henn.Vanus());

            //Inimene malle = new Inimene { IK = "45702280123", Nimi = "Malle Mallikas" };
            //Console.WriteLine(malle);

            //Inimene juku = new Inimene { IK = "51804010001", Nimi = "Juku" };
            //Console.WriteLine(juku);

            List<Person> people = new List<Person> { henn };
            ReadPeople("..\\..\\Õpetajad.txt", people);
            ReadPeople("..\\..\\Õpilased.txt", people);


            Console.WriteLine("\nJärgmise kuu sünnipäevalapsed\n");
            foreach (var p in people) 
                if(p.BirthDate.Month == DateTime.Now.Month + 1) // tsükime need, kel JÄRGMISEL kuul süna
                Console.WriteLine(p);

            DateTime startNextWeek = DateTime.Today.AddDays(7 -((int)(DateTime.Today.DayOfWeek) + 6) % 7);
            DateTime stopNextWeek = startNextWeek.AddDays(7);

            // kuidas leida järgmise nädala sünnipäevalapsed - keeruline
            Console.WriteLine("\nJärgmise nädala sünnipäevalapsed\n");
            foreach(var p in people)
            {
                DateTime sünnipäev = p.BirthDate.AddYears(p.Age + 1);
                if (sünnipäev >= startNextWeek && sünnipäev < stopNextWeek) Console.WriteLine(p);
            }

            // järgmise kuu jooksul süõnnipäevad
            Console.WriteLine("\njärgmise kuu aja jooksul on sellised sünnipäevad\n");
            DateTime algus = DateTime.Today;
            DateTime lõpp = algus.AddMonths(1);
            foreach (var p in people)
            {
                DateTime sünnipäev = p.BirthDate.AddYears(p.Age + 1);
                if (sünnipäev >= algus && sünnipäev < lõpp) Console.WriteLine(p);
            }

            Console.WriteLine("\nDatetime add months test\n");
            foreach(var d in new DateTime[] 
            {
                new DateTime(2019, 1, 28),
                new DateTime(2020, 1, 28),
                new DateTime(2019, 1, 29),
                new DateTime(2020, 1, 29),
                new DateTime(2019, 1, 30),
                new DateTime(2020, 1, 30),
            })
            {
                Console.WriteLine($"täna {d:dd.MM.yy} kuu pärast {d.AddMonths(1):dd.MM.yy}");
            }

        }

        static void ReadPeople(string filename, List<Person> list)
        {
            foreach(var rida in filename.ReadAllLines())
            {
                var osad = rida.Split(',');
                if (osad.Length > 1)
                list.Add(new Person { IK = osad[0].Trim(), Nimi = osad[1].Trim() });
            }
        }
    }
}
