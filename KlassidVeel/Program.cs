﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace KlassidVeel
{
    enum Mast { Risti = 1, Ruutu, Ärtu, Poti, Pada = 4 }

    [Flags]
    enum Tunnus { Suur = 1, Punane = 2, Puust = 4, Kolisev = 8}


    static class Program
    {
        static void Main()
        {

            Tunnus t = Tunnus.Suur | Tunnus.Kolisev;
            t |= Tunnus.Punane;
            Console.WriteLine(t);

            Mast m = Mast.Risti;
            m++;
            Console.WriteLine((int)m);
            Console.WriteLine((Mast)3);


            Inimene henn = new Inimene { Nimi = "henn sarv", Palk = 8000 };
            Console.WriteLine(henn);

            henn.SetPalk(10000);
            Console.WriteLine(henn.GetPalk());
            henn.SetPalk(5000);
            Console.WriteLine(henn.GetPalk());

            Console.WriteLine(henn.Palk);
            henn.Palk = henn.Palk + 500;

            // mõned näidislauseid neile, kellele mängida meeldib -  vaata uuri, kuidas see toimib
            Inimene boss = ("Karla", 2000);
            Console.WriteLine(boss);

            Inimene malle = "Malle";
            Inimene kalle = "Kalle";
            Inimene juku = kalle + malle;
            juku.Nimi = "Juku";
            Console.WriteLine($"{juku} - isa on {juku.Isa.Nimi} ema on {juku.Ema.Nimi}");
            (boss + malle).Nimi = "Miku";
            Console.WriteLine("malle lapsed");
            foreach(var l in malle.Lapsed)
                Console.WriteLine($"{l.Nimi} - isa {l.Isa.Nimi}");


        }


        static void MainVana()
        {
            // tegin uue Main-i omale, et ei peaks kustutama ega uuesti alustama

            Console.WriteLine(Inimene.ToProper("henn")); // hennuga toimis
            Console.WriteLine(Inimene.ToProper(""));     // ahaa - tühja nimega ei toimi - nüüd toimib
            Console.WriteLine(Inimene.ToProper("jorch-adniel kiir"));   // mitmeosalisega toimib, aga sidekriipsuga ei
                                                                        // ma sain sidekriipsuga ka toimima
                                                                        // kes on laisk, võib seda kasutada :) aga kes tahab õppida, proovib ise teha
                                                                        //TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
                                                                        //Console.WriteLine(ti.ToTitleCase("jorch-adniel kiir"));

            // Console.WriteLine(Inimene.ToProper("o'brien")) -- nii läheb segaseks, seda me ei proovi

            Inimene henn = new Inimene { Nimi = "henn sarv" }; // see on, milleni me tahaks jõuda peale lõunat
            Console.WriteLine(henn); // 1. Henn Sarv sündinud 01.jaanuar.1801
            // et inimese nimi hoitakse automaatselt alati suurte tähtedega

        }

        // vana maini nimetasin ringi
        static void MainEelmine(string[] args)
        {
            Inimene henn = new Inimene { Nimi = "Henn Sarv", IK = "35503070211" };
            //Console.WriteLine(henn);

            Inimene ants = new Inimene { Nimi = "vana Ants" };
            //Console.WriteLine(ants.Sünniaeg());
            ants = new Inimene { Nimi = "noor Ants" }; // sellel hetkel vana ants UNUSTATAKSE

            foreach (var i in Inimene.Inimesed) Console.WriteLine(i);
            Console.WriteLine(Liida(100));  // y =0 ja z = 0
            Console.WriteLine(Liida(4, 7));         // positsiooniline z = 0
            Console.WriteLine(Liida(x : 4, z : 7)); // nimelised y = 0

            Console.WriteLine(LiidaX());
            Console.WriteLine(LiidaX(1,2,3,4,5,6,7));

        }

        static int Liida(int x, int y = 0, int z = 0)
        {
            return 4*x + 2*y + z;
        }

        static int LiidaX(params int[] muud)
        {
            int sum = 0;
            foreach (var s in muud) sum += s;
            return sum;
        }

        #region tegime ühe halva overloadmise näite
        //static int Liida(int x, int y, int z)
        //{
        //    return x + y + z;
        //}

        //static double Liida(double d1, double d2)
        //{
        //    return d1 + d2 + 100;

        //}
        //static double Liida(int d1, double d2)
        //{
        //    return d1 + d2 + 200;

        //}
        //static double Liida(double d1, int d2)
        //{
        //    return d1 + d2 + 300;

        //} 
        #endregion
    }
}
