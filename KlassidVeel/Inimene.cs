﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidVeel
{
    enum Sugu { Naine, Mees}
    enum LapseSugu { Tüdruk, Poiss}
    class Inimene
    {
        static int loendur = 0; // viisakas on näidata algväärtus
        static List<Inimene> inimesed = new List<Inimene>();

        // reedel vaatame, mis asi see IEnumerable on
        public static IEnumerable<Inimene> Inimesed => inimesed.AsEnumerable();

        private string _Nimi = "";
        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = ToProper(value);
        }

        public string IK = "101010100000";

        public int Nr { get; } = ++loendur;

        private decimal _Palk;

        public decimal GetPalk() => _Palk;   // getter-funktsioon
        public void SetPalk(decimal palk) => _Palk = palk > _Palk ? palk : _Palk; // setter-meetod

        // Java tunnis jätaks me selle kohal loo pooleli
        // aga meil on veel üks variant

        // property - on setter-getter paar, mis näeb välja nagu field
        public decimal Palk
        {
            get
            {
                return _Palk;
            }
            internal set
            {
                _Palk = value > _Palk ? value : _Palk;
            }
        }


        public Inimene()                // voidi ei ole, klassi nimega sama nimi
        {
            inimesed.Add(this);
        }

        public Sugu Sugu => (Sugu)(IK[0] % 2);
        public LapseSugu LapseSugu = (LapseSugu)(IK[0] % 2);
        public string Kes => Vanus < 18 ? LapseSugu.ToString() : Sugu.ToString();

        public int Vanus => (DateTime.Today - Sünniaeg).Days * 4 / 1461;

        public DateTime Sünniaeg
         =>
                (IK.Length < 11) ? new DateTime() : // 

                new DateTime(
               (IK[0] == '1' || IK[0] == '2' ? 1800 :
                IK[0] == '3' || IK[0] == '4' ? 1900 : 2000) +
                int.Parse(IK.Substring(1, 2)),
                int.Parse(IK.Substring(3, 2)),
                int.Parse(IK.Substring(5, 2))
                );


        public static string ToProper(string nimi)
        {
            // ma tegelikult tahaks siit kommentaarid ära korjata, siis oleks kood ilusam

            // kuidas teha üks nimi suure algustähega? on kellelgi ideid
            // esitäht suureks ja ülejäänud väikseks? kas nii võiks
            // kuidas saada esitäht, kuidas saada ülejäänud, kuidas saada suureks/väikseks

            // nüüd oleks vaja mitme nimega sama asja? kuidas seda teha
            // nimi tükeldada ja iga tükk teha suureks? Hmmm....
            // pärast vaja uuesti kokku ka panna
            // tükeldada - kuidas?      Split vist tükeldas
            // kokku panna - kuidas?    string.Join kui meelde tuletada, pani kokku

            var nimed = nimi.Replace("-", "- ").Split(' '); // nii sai tükeldada
            // nüüd siin vahepeal tuleks teha selles massiivis olevate asjadega nii nagu enne
            // ähk teeks tsükli? üle selle massiivi? kuidas seda teha??
            // foreach või for? mida foreachiga ei saanud teha
            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed[i] == "" ? "" : // nüüd peaks tühja nimega ka toimima
                nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1).ToLower();
            }

            return string.Join(" ", nimed).Replace("- ", "-"); // ja nii saab kokku panna
                                                               // nüüd oleks vaja mõelda, mis teha, kui -ga nimed on sehes


            // proovime!
        }


        // override teema jätame homseks
        public override string ToString() { return $"{Nr}. {Nimi} sündinud {Sünniaeg:dd.MMMM.yyy}"; }

        // lisan mõned väljad ja tehted, mängimise mõttes - seda siit loe ainult omal vastustusel
        // mina seletada ei oska
        // ära seda tõsiselt võta


        // üks asi, mida me ei õpi - tehted e. operaatorid
        // see siin on teisendusoperaator
        public static implicit operator Inimene((string nimi, decimal palk) inimene) 
            => new Inimene { Nimi = inimene.nimi, Palk = inimene.palk};

        public static implicit operator Inimene(string nimi) => new Inimene { Nimi = nimi};

        public Inimene Ema;             // jätan kõik kolm pragu public
        public Inimene Isa;
        public List<Inimene> Lapsed = new List<Inimene>();

        public static Inimene operator +(Inimene isa, Inimene ema)
        {
            Inimene laps = new Inimene { Isa = isa, Ema = ema };
            isa.Lapsed.Add(laps);
            ema.Lapsed.Add(laps);
            return laps;
        }
    }
}
